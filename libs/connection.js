var mysql = require('mysql'),
    mysqlUtilities = require('mysql-utilities');
var now = new Date();

var my = {
    "host":     "localhost",
        "user":     "root",
        "password": "707",
        "database": "modis_elite"
};

var connection = mysql.createConnection(my);
connection.query("SET NAMES utf8");
mysqlUtilities.upgrade(connection);
mysqlUtilities.introspection(connection);
function replaceClientOnDisconnect(connection) {
    connection.on("error", function (err) {
        if (!err.fatal) {
            return;
        }
        if (err.code !== "PROTOCOL_CONNECTION_LOST") {
            throw err;
        }
        connection = mysql.createConnection(my);
        connection.query("SET NAMES utf8");
        mysqlUtilities.upgrade(connection);
        mysqlUtilities.introspection(connection);
        replaceClientOnDisconnect(connection);
        connection.connect(function (error) {
            console.log(now.getHours() + ':' + now.getMinutes() + ' | Переподключили муську');
            if (error) {
                console.log(now.getHours() + ':' + now.getMinutes() +  ' | Ошибка переподключения муськи' + error);
            }
        });
    });
}
replaceClientOnDisconnect(connection);

var timerMysql = setInterval(function() {
    connection.query('SELECT 1');
}, 10000);

console.log(now.getHours() + ':' + now.getMinutes() + ' | Подключили муську');

module.exports.mysql = connection;