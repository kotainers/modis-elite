/**
 * Created by kotainer on 28.10.2016.
 */
var connection = require('../libs/connection').mysql;

getGroupForCp = function (Client, cp_id) {
    try{
        var sqlstr='call getGroupForCp(?)';
        connection.query(sqlstr, cp_id, function (err,cp) {
            if(!err)
                Client.send(JSON.stringify(
                    {
                        act: "groupList",
                        data: cp[0],
                        cp_id: cp_id
                    }));
            else {
                console.log(err);
            }
        });
    }catch(e) {
        console.error('getGroupForCp - ' + e);
    };
};

getStudentList = function (Client, param) {
    try{
        var sqlstr='call getStudentList(?,?)';
        var p = [param.group_id, param.cp_id];
        connection.query(sqlstr, p, function (err,cp) {
            if(!err)
                Client.send(JSON.stringify(
                    {
                        act: "studentList",
                        data: cp[0],
                        group_id: param.group_id
                    }));
            else {
                console.log(err);
            }
        });
    }catch(e) {
        console.error('getStudentList - ' + e);
    };
};

module.exports.getGroupForCp = getGroupForCp;
module.exports.getStudentList = getStudentList;