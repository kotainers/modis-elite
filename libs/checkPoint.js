var connection = require('../libs/connection').mysql;

getCpForDiscip = function (Client, param) {
    try{
        var sqlstr='call getEliteCpForDiscip(?,?)';
        var p = [param.id, param.discip_id];
        connection.query(sqlstr,p, function (err,cp) {
            if(!err)
                Client.send(JSON.stringify(
                    {
                        act: "cpList",
                        data: cp[0],
                        discip_id: param.discip_id
                    }));
            else {
                console.log(err);
            }
        });
    }catch(e) {
        console.error('getCpForDiscip - ' + e);
    };
};

addCpForDiscip = function (Client, param) {
    try{
        var sqlstr='call getEliteCpForDiscip(?,?)';
        var p = [param.id, param.discip_id];
        connection.query(sqlstr,p, function (err,cp) {
            if(!err)
                Client.send(JSON.stringify(
                    {
                        act: "cpList",
                        data: cp[0],
                        discip_id: param.discip_id
                    }));
            else {
                console.log(err);
            }
        });
    }catch(e) {
        console.error('addCpForDiscip - ' + e);
    };
};

module.exports.getCpForDiscip = getCpForDiscip;
module.exports.addCpForDiscip = addCpForDiscip;
