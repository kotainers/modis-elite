var WebSocketServer = new require('ws');
var discipline = require('../libs/discip');
var checkPoint = require('../libs/checkPoint');
var group = require('../libs/group');

var webSocketServer = new WebSocketServer.Server({port: 8987});
webSocketServer.on('connection', function(ws) {
    ws.on('message', function(message) {
        try{
            var msg = JSON.parse(message);
            console.log('Действие: ' + message);

            if (msg.act == 'getDiscipList') {discipline.getDiscipList(ws)};

            if (msg.act == 'getCpForDiscip') {checkPoint.getCpForDiscip(ws, msg)};

            if (msg.act == 'getGroupForCp') {group.getGroupForCp(ws, msg.cp_id)};

            if (msg.act == 'getStudentList') {group.getStudentList(ws, msg)};

        }
        catch(e) {
            console.error(e);
        }
    });

    ws.on('close', function() {
        delete ws;
    });
});
console.log('WS запущен на порту 8987');