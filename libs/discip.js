var connection = require('../libs/connection').mysql;

getDiscipList = function (Client) {
    try{
        var sqlstr='call getEliteDiscip()';
        connection.query(sqlstr, function (err,d) {
            if(!err)
                Client.send(JSON.stringify(
                    {
                        act: "discipList",
                        data: d[0]
                    }));
            else {
                console.log(err);
            }
        });
    }catch(e) {
        console.error('getFirmList - ' + e);
    };
};

module.exports.getDiscipList = getDiscipList;
