var ws = new WebSocket("ws://localhost:8987");
angular.module('elite', ['ngAnimate', 'ngSanitize', 'ui.bootstrap']).controller('EliteCtrl', function ($scope, $uibModal, $log) {

//angular.module('elite').controller('EliteCtrl', function ($scope, $modal) {

    $scope.newCp = {
        date: '',
        weight: 0
    };

    $scope.openModalAdd = function () {
        $uibModal.open({
            templateUrl: 'myModalContent.html',
            backdrop: true,
            windowClass: 'modal',
            controller: function ($scope, $uibModalInstance, $log, newCp) {
                $scope.newCp = newCp;
                $scope.submit = function () {
                    $log.log('Submiting user info.');
                    $log.log(newCp);
                    $uibModalInstance.dismiss('cancel');
                }
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };

                // Дата
                $scope.today = function () {
                    $scope.dt = new Date();
                };
                $scope.today();
                $scope.clear = function () {
                    $scope.dt = null;
                };
                $scope.inlineOptions = {
                    customClass: getDayClass,
                    minDate: new Date(),
                    showWeeks: true
                };
                $scope.dateOptions = {
                    dateDisabled: disabled,
                    formatYear: 'yy',
                    maxDate: new Date(2020, 5, 22),
                    minDate: new Date(),
                    startingDay: 1
                };
                // Disable weekend selection
                function disabled(data) {
                    var date = data.date,
                        mode = data.mode;
                    return mode === 'day' && (date.getDay() === 0 );
                }

                $scope.toggleMin = function () {
                    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
                    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
                };
                $scope.toggleMin();
                $scope.open = function () {
                    $scope.popup.opened = true;
                };
                $scope.setDate = function (year, month, day) {
                    $scope.dt = new Date(year, month, day);
                };
                $scope.format = 'dd.MM.yyyy';
                $scope.altInputFormats = ['dd.MM.yyyy'];
                $scope.popup = {
                    opened: false
                };
                var tomorrow = new Date();
                tomorrow.setDate(tomorrow.getDate() + 1);
                var afterTomorrow = new Date();
                afterTomorrow.setDate(tomorrow.getDate() + 1);
                $scope.events = [
                    {
                        date: tomorrow,
                        status: 'full'
                    },
                    {
                        date: afterTomorrow,
                        status: 'partially'
                    }
                ];

                function getDayClass(data) {
                    var date = data.date,
                        mode = data.mode;
                    if (mode === 'day') {
                        var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                        for (var i = 0; i < $scope.events.length; i++) {
                            var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                            if (dayToCheck === currentDay) {
                                return $scope.events[i].status;
                            }
                        }
                    }

                    return '';
                }

            },
            resolve: {
                newCp: function () {
                    return $scope.newCp;
                }
            }
        });
    };

    $scope.isCollapsed = [];
    $scope.checkPoint = [];
    $scope.groups = [];
    $scope.students = [];

    ws.onopen = function () {
        ws.send(JSON.stringify({
            act: 'getDiscipList'
        }));
    };

    ws.onmessage = function (event) {
        var msg = JSON.parse(event.data);
        if (msg.act == 'discipList') {
            if (!$scope.$$phase) $scope.$apply(function () {
                $scope.discipList = msg.data;
            });
        }
        if (msg.act == 'cpList') {
            if (!$scope.$$phase) $scope.$apply(function () {
                $scope.checkPoint[msg.discip_id] = msg.data;
            });
        }
        if (msg.act == 'groupList') {
            if (!$scope.$$phase) $scope.$apply(function () {
                $scope.groups[msg.cp_id] = msg.data;
            });
        }
        if (msg.act == 'studentList') {
            if (!$scope.$$phase) $scope.$apply(function () {
                $scope.students[msg.group_id] = msg.data;
            });
        }
    };

    $scope.openDiscip = function (discip_id) {
        ws.send(JSON.stringify({
            act: 'getCpForDiscip',
            discip_id: discip_id,
            id: 1
        }));
        $scope.isCollapsed[discip_id] = !$scope.isCollapsed[discip_id];
    };

    $scope.openCP = function (cp_id) {
        ws.send(JSON.stringify({
            act: 'getGroupForCp',
            cp_id: cp_id
        }));
    };

    $scope.openGroup = function (g_id, cp_id) {
        ws.send(JSON.stringify({
            act: 'getStudentList',
            group_id: g_id,
            cp_id: cp_id
        }));
    };

});
//};